extern crate chrono;
extern crate crossbeam;
extern crate floating_duration;
extern crate futures;
extern crate governor;
extern crate reqwest;
extern crate tokio;
#[macro_use]
extern crate log;
extern crate env_logger;

use chrono::Local;
use floating_duration::TimeFormat;
use futures::{stream, StreamExt};
use governor::{Quota, RateLimiter};

use nonzero_ext::*;
use reqwest::{Client, Response};
use std::error::Error;
use std::process;
use std::time::Instant;

// use std::num::NonZeroU32;
// use futures::Stream;
// use async_stream::stream;
// use futures::executor::block_on;
// use tokio::runtime::Runtime;
// use futures::pin_mut;
// use governor::RatelimitedStream;
// use governor::state::direct::RatelimitedStream;
// use governor::state::direct::StreamRateLimitExt;

#[tokio::main]
async fn async_main() -> Result<(), Box<dyn Error>> {
    const LIMIT_CHAN: u32 = 5u32;
    let lim = RateLimiter::direct(Quota::per_second(nonzero!(LIMIT_CHAN)));

    let now = Instant::now();

    // for i in v {
    //     let body = reqwest::get("https://www.rust-lang.org")
    //         .await?
    //         .text()
    //         .await?;

    //     println!("{} \t {} \t {}", process::id(), TimeFormat(now.elapsed()), body.len());

    //     // lim.until_n_ready(nonzero!(5u32)).await;
    //     lim.until_ready().await;
    // }

    let client = Client::new();
    let urls = vec!["http://localhost:4000"; 40];
    const PARALLEL_REQUESTS: usize = LIMIT_CHAN as usize;

    info!("INIT async_main {}", Local::now().format("%+"));

    let s2 = stream::iter(urls)
        .map(|url| {
            let client = &client;
            async move {
                // let resp = client.get(url).send().await?;
                // resp.bytes().await
                // lim.until_ready().await;
                client.get(url).send().await
            }
        })
        .buffer_unordered(PARALLEL_REQUESTS);

    // the bucket is initially full
    // make the bucket empty
    info!("RSTREAM async_main {}", Local::now().format("%+"));

    for _i in 0..LIMIT_CHAN {
        lim.until_ready().await;
    }

    info!("BUCKET_EMPTY async_main {}", Local::now().format("%+"));

    s2.for_each(|b| {
        async {
            // delay request to honour ratelimit
            &lim.until_ready().await;

            // match b {
            //     Ok(b) => println!("{} bytes \t {}", b.len(), TimeFormat(now.elapsed())),
            //     Err(e) => eprintln!("Got an error: {}", e),
            // }
            info!("{}\t{}", b.unwrap().status(), TimeFormat(now.elapsed()));
        }
    })
    .await;

    Ok(())
}

fn main() {
    env_logger::init();

    info!("pid {}", process::id());

    let now = Instant::now();

    async_main();

    info!("COMPLETED async_main {}", TimeFormat(now.elapsed()));
}
