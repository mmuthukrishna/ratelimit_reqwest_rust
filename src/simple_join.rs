extern crate futures;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate reqwest;

use std::{thread, time};
use futures::executor::block_on;
use std::process;
// use log::Level;

async fn another_one(index: i32){
    // let ten_millis = time::Duration::from_millis(1000);
    // thread::sleep(ten_millis);

    let body = reqwest::get("https://www.rust-lang.org");

    info!("another one {} {}", index, process::id());
}

async fn async_main(){
    let f1 = another_one(1);
    let f2 = another_one(2);

    // block_on(f1);
    // block_on(f2);

    futures::join!(f1, f2);
}

fn main() {
    env_logger::init();

    let f = async_main();

    block_on(f);
}
