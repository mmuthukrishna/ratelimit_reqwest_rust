# async_book examples

Using tokio async, reqwest and governor to make async requests that honour a specified rate limit.

## Usage Instructions

1. Execute `go run ratelimit_go/*` to create a simple web server with ratelimit of 10 requests/sec.
2. Execute `RUST_LOG=info cargo run` to hit this endpoint.
